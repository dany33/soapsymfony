<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\SoapDataService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;




class DefaultController extends AbstractController
{

    /**
     * @Route("/index", methods={"GET"}, name="default_index")
     */
    public function index(Request $request): Response
    {


        return $this->render('default/index.html.twig', []);
    }




    /**
     * @Route("/recept", methods={"GET"}, name="default_recept")
     */
    public function recept(Request $request, ParameterBagInterface $params, SoapDataService $soapDataService): Response
    {
        $filename = $params->get('root_dir') . "/../public/hello.wsdl";

/*
        // genere un fichier wsdl à partir d'un objet
        $gen = new \PHP2WSDL\PHPClass2WSDL($soapDataService, "http://127.0.0.1:8000/recept");
        $gen->generateWSDL();
        $gen->save($filename);
*/

        $soapServer = new \SoapServer($filename);
        $soapServer->setObject($soapDataService);
        $soapServer->setClass(get_class($soapDataService));

        $response = new Response();
        $response->headers->set('Content-Type', 'text/xml; charset=ISO-8859-1');

        ob_start();
        $soapServer->handle();

        $response->setContent(ob_get_clean());

        return $response;
    }



    /**
     * @Route("/client", methods={"GET"}, name="default_client")
     */
    public function client(Request $request, ParameterBagInterface $params): Response
    {
        try {
            $soapClient1 = new \SoapClient('http://loc.soap.fr/index.php/recept?wsdl');

            $return = new Response(var_dump($soapClient1->hello('duke')));


        } catch (\SoapFault $e) {
            return new Response(var_dump($e));
        }
        
        return new Response($return);
    }
}


