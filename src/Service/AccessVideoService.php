<?php
/**
 * Created by PhpStorm.
 * User: kevin.martin
 * Date: 12/02/2019
 * Time: 08:39
 */

namespace App\Service;

use App\Entity\Video;
use App\Repository\CategoryRepository;
use App\Repository\SubCategoryRepository;


class AccessVideoService
{
    private $categoryRepository;
    private $subCategoryRepository;

    public function __construct(CategoryRepository $categoryRepository, SubCategoryRepository $subCategoryRepository)
    {

        $this->categoryRepository = $categoryRepository;
        $this->subCategoryRepository = $subCategoryRepository;
    }



    public function getCategoryList() {
        $arrCategoryList = [];
        foreach ($this->categoryRepository->findBy(array(), array('name' => 'ASC')) as $category) {
            $arrCategoryList[$category->getName()] = [];
            foreach ($category->getSubCategories() as $subcategory) {
                $arrCategoryList[$category->getName()][$subcategory->getName()] = $subcategory->getId();
            }
        }

        return $arrCategoryList;
    }


    public function getVideoToAdd($videosFromVimeo, $videosFromBase)
    {
        $videoToAdd = array();
        foreach ($videosFromVimeo as $video) {
            $finding = false;
            foreach ($videosFromBase as $value) {
                if ($value->getVimeoResourceKey() == $video['resource_key']) {
                    $finding = true;
                }
            }
            if (!$finding) {
                $videoToAdd[] = $video;
            }
        }

        return $videoToAdd;
    }


    public function getVideoRemoved($videosFromVimeo, $videosFromBase)
    {
        $videoRemoved = array();
        foreach ($videosFromBase as $video) {
            $finding = false;
            foreach ($videosFromVimeo as $value) {
                if ($video->getVimeoResourceKey() == $value['resource_key']) {
                    $finding = true;
                }
            }
            if (!$finding) {
                $videoRemoved[] = $video;
            }
        }

        return $videoRemoved;
    }

    public function createVideo($videosFromVimeo, $resource_key, $idSubCategorie) {

        $subCategpory = $this->subCategoryRepository->findOneById($idSubCategorie);


        $video = new Video();
        foreach ($videosFromVimeo as $videoFromViemo) {

            if ($videoFromViemo['resource_key'] == $resource_key) {

                $video->setName($videoFromViemo['name']);
                $video->setDescription($videoFromViemo['description']);
                $video->setSubcategory($subCategpory);

                $video->setUrl($videoFromViemo['link']);
                $video->setVimeoResourceKey($resource_key);
                $video->setVimeoCategories($videoFromViemo['categories']);
                $video->setVimeoDuration($videoFromViemo['duration']);
                $video->setVimeoName($videoFromViemo['name']);
                $video->setVimeoPictures($videoFromViemo['pictures']);
                $video->setVimeoPictureLink($videoFromViemo['pictures']['sizes'][1]['link']);
                $video->setVimeoPictureLinkWithPlayButton($videoFromViemo['pictures']['sizes'][1]['link_with_play_button']);
                $video->setVimeoStatus($videoFromViemo['status']);
                $video->setVimeoTags($videoFromViemo['tags']);
                $video->setVimeoVideoHeight($videoFromViemo['height']);
                $video->setVimeoVideoWidth($videoFromViemo['width']);
                $video->setVimeoDescription($videoFromViemo['description']);
                $video->setVimeoUri($videoFromViemo['uri']);

                return $video;
            }
        }
    }



}